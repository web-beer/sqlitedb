import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"


AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const product = new Product()
    const productRepository =AppDataSource.getRepository(Product)
    product.name ="ข้าวมันไก่";
    product.price = 50;
   
    await AppDataSource.manager.save(product)

    console.log("Saved a new user with id: " + product.id)

    console.log("Loading product from the database...")
    const users = await productRepository.find()
    console.log("Loaded users: ", users)
}).catch(error => console.log(error))
